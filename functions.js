$(document).ready(function(){
    
    $('.cta a').click(function() {
        $('html,body').animate({
            scrollTop: $('#tours').offset().top
        }, 500);
        return false;
    });
    
    $('#tours li').on('click', function() {
        $('#location').val($('img', this).attr('alt'));
    });

    // function to show modal
    function show_modal(cover, excerpt) {
        $('.modal img').attr('src', cover);
        $('.modal p').text(excerpt);
        document.getElementById('maskcontent').style.display = 'block';
    }
    // fetching data from the link
    $.ajax({
        url : 'https://9ss7bxey8k.execute-api.ap-southeast-2.amazonaws.com/default/dummy_service',
        success : function(result) {            
            let data = result.Data;            
            for (item of data) {
                let excerpt = item.node.excerpt;
                let title = item.node.frontmatter.title;
                let cover = item.node.frontmatter.cover;
                let itemobj = document.createElement('li');
                itemobj.innerHTML = `
                    <div>
                        <h3>${excerpt}</h3>
                        <h4>${title}</h4>
                    </div>
                    <img alt='Saint Mary Lake, Montana' src=${cover}>
                `;
                itemobj.addEventListener('click', () => show_modal(cover, excerpt));
                $('#imagecontent').append(itemobj);
            }
            for (let i=0 ; i<4 - data.length%4 ; i++) {
                let template = '<li></li>';
                $('#imagecontent').append(template);
            }        
        }
    });
})